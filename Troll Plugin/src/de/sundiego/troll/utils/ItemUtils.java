package de.sundiego.troll.utils;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class ItemUtils {
	
	public static ItemStack createSkull(String playername) {
		ItemStack stack = new ItemStack(Material.SKULL);
		
		SkullMeta meta = (SkullMeta)stack.getItemMeta();
		OfflinePlayer player = (OfflinePlayer)Bukkit.getPlayer(playername);
		meta.setOwningPlayer(player);
		stack.setItemMeta(meta);
		
		return stack;
	}
	
	public static ItemStack createItemStack(Material mat, String name) {
		return createItemStack(mat, 1, name, false);
	}
	
	public static ItemStack createItemStack(Material mat, int amount, String name, boolean unbreakable) {
		return createItemStack(mat, amount, (byte)0, name, null, unbreakable);
	}
	
	public static ItemStack createItemStack(Material mat, int amount, byte data, boolean unbreakable) {
		return createItemStack(mat, amount, data, null, null, unbreakable);
	}
	
	public static ItemStack createItemStack(Material mat, int amount, byte data, List<String> lore, boolean unbreakable) {
		return createItemStack(mat, amount, data, null, lore, unbreakable);
	}
	
	public static ItemStack createItemStack(Material mat, int amount, byte data, List<String> lore) {
		return createItemStack(mat, amount, data, null, lore, false);
	}
	
	public static ItemStack createItemStack(Material mat, int amount, byte data, String name) {
		return createItemStack(mat, amount, data, name, null, false);
	}
	
	public static ItemStack createItemStack(Material mat, int amount, byte data, String name, List<String> lore, boolean unbreakable) {
		ItemStack stack = new ItemStack(mat, amount, data);
		
		ItemMeta meta = stack.getItemMeta();
		if(name != null) {
			meta.setDisplayName(name);
		}
		if(lore.isEmpty() && lore != null ) {
			meta.setLore(lore);
		}
			meta.setUnbreakable(unbreakable);
		stack.setItemMeta(meta);
		
		return stack;
	}
	
}
