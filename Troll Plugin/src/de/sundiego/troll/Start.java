package de.sundiego.troll;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import de.sundiego.troll.commands.DemoscreenCommand;
import de.sundiego.troll.commands.ExplosionBowCommand;
import de.sundiego.troll.commands.FakeOpCommand;
import de.sundiego.troll.commands.FreezeCommand;
import de.sundiego.troll.commands.ICustomCommand;
import de.sundiego.troll.commands.TNTSchockCommand;

public class Start extends JavaPlugin {
	
	public static Start instance = null;
	
	public String prefix = "�6[�eTroll�6] �7";
	
	public ArrayList<ICustomCommand> commands = new ArrayList<ICustomCommand>();
	
	public void onEnable() {
		instance = this;
		commands.add(new DemoscreenCommand());
		commands.add(new ExplosionBowCommand());
		commands.add(new FakeOpCommand());
		commands.add(new FreezeCommand());
		commands.add(new TNTSchockCommand());
		
		initListeners();
	}
	
	private void initListeners( ) {
		for(ICustomCommand command : commands) {
			Bukkit.getPluginManager().registerEvents(command, instance);
		}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		for(ICustomCommand cmd : commands) {
			if(cmd.getName().equals(command.getName())) {
				cmd.execute(sender, command, label, args);
			}
		}
		
		return super.onCommand(sender, command, label, args);
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		
		for(ICustomCommand cmd : commands) {
			if(cmd.getName().equals(command.getName())) {
				return cmd.onTabComplete(sender, command, alias, args);
			}
		}
		
		return super.onTabComplete(sender, command, alias, args);
	}
}
