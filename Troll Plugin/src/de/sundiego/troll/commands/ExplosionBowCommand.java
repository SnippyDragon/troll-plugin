package de.sundiego.troll.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityShootBowEvent;

import de.sundiego.troll.Start;
import de.sundiego.troll.vars.Bows;

public class ExplosionBowCommand implements ICustomCommand {

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "explosionbow";
	}

	@Override
	public String getDesc() {
		// TODO Auto-generated method stub
		return "Gibt dir einen Bogen, wo der Pfeil Explodiert!";
	}

	@Override
	public String getUsage() {
		// TODO Auto-generated method stub
		return "-";
	}

	@Override
	public void execute(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player ) {
			Player p = (Player)sender;
			if(p.hasPermission("troll.commands.explosionbow")) {
				p.getInventory().addItem(Bows.EXPLOSION_BOW);
				p.sendMessage(Start.instance.prefix + "�aDu hast jetzt einen Explosiven Bogen!");
			} else {
				p.sendMessage(Start.instance.prefix + "�cDu hast nicht die Rechte um diesen Befehl auszuf�hren!");
			}
		}
	}
	
	
	@EventHandler
	public void onArrowShoot(EntityShootBowEvent e) {
		if((e.getEntity() instanceof Player)) {
			Player p = (Player)e.getEntity();
			if(p.hasPermission("troll.bows.explosion") && e.getBow().equals(Bows.EXPLOSION_BOW)) {
				Arrow a = (Arrow)e.getProjectile();
				a.getLocation().getWorld().createExplosion(a.getLocation(), 6.0F, true);
			}
		}
	}
	
	

}
