package de.sundiego.troll.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.sundiego.troll.Start;

public class TNTSchockCommand implements ICustomCommand {

	@Override
	public String getName() {
		return "tntschock";
	}

	@Override
	public String getDesc() {
		return "Der Spieler denkt dass um ihm TNT ist.";
	}

	@Override
	public String getUsage() {
		return "<Spieler>";
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(p.hasPermission("troll.commands.tntschock")) {
				if(args.length == 1) {
					try {
						Player vic = Bukkit.getPlayer(args[0]);
						for(double x = vic.getLocation().getX() - 10.0D; x <= vic.getLocation().getX() + 10.0D; x += 1.0D) {
							for(double y = vic.getLocation().getY() - 10.0D; y <= vic.getLocation().getY() + 10.0D; y += 1.0D) {
								for(double z = vic.getLocation().getZ() - 10.0D; z <= vic.getLocation().getZ() + 10.0D; z += 1.0D) {
									Location l = new Location(vic.getWorld(), x, y, z);
									if(l.getBlock().getType() != Material.AIR) {
										vic.sendBlockChange(l, Material.TNT, (byte)0);
									}
								}
							}
						}
					} catch (Exception e) {
						p.sendMessage(Start.instance.prefix + "�cDieser Spieler ist leider nicht Online!");
					}
				}
			} else {
				p.sendMessage(Start.instance.prefix + "�cDu hast nicht die Rechte um diesen Befehl auszuf�hren!");
			}
		}
	}

}
