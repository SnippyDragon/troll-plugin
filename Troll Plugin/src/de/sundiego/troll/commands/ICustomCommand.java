package de.sundiego.troll.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;

public interface ICustomCommand extends Listener {

	String getName();
	
	String getDesc();
	
	String getUsage();
	
	void execute(CommandSender sender,Command command, String label, String[] args);
	
	default List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {  return new ArrayList<String>(); }
	
	
	
}
