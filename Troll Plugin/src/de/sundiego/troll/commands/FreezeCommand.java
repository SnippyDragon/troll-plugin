package de.sundiego.troll.commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

import de.sundiego.troll.Start;

public class FreezeCommand implements ICustomCommand {

	public ArrayList<Player> FreezedPlayers = new ArrayList<Player>();
	
	@Override
	public String getName() {
		return "freeze";
	}

	@Override
	public String getDesc() {
		return "Friert einen Spieler ein!";
	}

	@Override
	public String getUsage() {
		return "<Spieler>";
	}

	@Override
	public void execute(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player)  {
			Player p = (Player)sender;
			if(p.hasPermission("troll.commands.freeze") ) {
				if(args.length == 1) {
					try {
						Player vic = Bukkit.getPlayer(args[0]);
						if(FreezedPlayers.contains(vic)) {
							FreezedPlayers.remove(vic);
							p.sendMessage(Start.instance.prefix + "�aDer Spieler kann sich jetzt wieder Bewegen!");
						} else {
							FreezedPlayers.add(vic);
							p.sendMessage(Start.instance.prefix + "�aDer Spieler kann sich jetzt nicht mehr Bewegen!");
						}
					}catch (Exception e) {
						p.sendMessage(Start.instance.prefix + "�cDer Spieler ist leider nicht Online!");
					}
				}
			} else {
				p.sendMessage(Start.instance.prefix + "�cDu hast leider nicht die Rechte um diesen Befehl auszuf�hren!");
			}
		}
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		for(Player all : FreezedPlayers) {
			e.setCancelled(true);
			all.teleport(e.getFrom());
		}
	}

}
