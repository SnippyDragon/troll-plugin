package de.sundiego.troll.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import de.sundiego.troll.Start;
import net.minecraft.server.v1_12_R1.PacketPlayOutGameStateChange;

public class DemoscreenCommand implements ICustomCommand {

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "demoscreen";
	}

	@Override
	public String getDesc() {
		// TODO Auto-generated method stub
		return "Sendet einen Demoscreen an einen Spieler";
	}
	
	public String getUsage() {
		return "<Spieler>";
	}

	@Override
	public void execute(CommandSender sender, Command command, String label, String[] args) {

		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(p.hasPermission("troll.command.demoscreen")) {
				if(args.length == 1) {
					try {
						Player vic = Bukkit.getPlayer(args[0]);
						((CraftPlayer)vic).getHandle().playerConnection.sendPacket(new PacketPlayOutGameStateChange(5, 0));
						p.sendMessage(Start.instance.prefix + "�aDer Spieler sieht jetzt einen Demoscreen!");
					} catch (Exception e) {
						p.sendMessage(Start.instance.prefix + "�cDer Spieler ist leider nicht Online!");
					}
				} else {
					p.sendMessage(Start.instance.prefix + "�7Usage: ");
					p.sendMessage(Start.instance.prefix + "�7/�8" + getName() + " �a" + getUsage());
				}
			} else {
				p.sendMessage(Start.instance.prefix + "�cDu hast nicht die Rechte um diesen Befehl auszuf�hren!");
			}
		}
		
	}

}
