package de.sundiego.troll.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.sundiego.troll.Start;

public class FakeOpCommand implements ICustomCommand {

	@Override
	public String getName() {
		return "fakeop";
	}

	@Override
	public String getDesc() {
		return "Gibt einem Spieler Fake OP Rechte";
	}

	@Override
	public String getUsage() {
		return "<Spieler>";
	}

	@Override
	public void execute(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(p.hasPermission("troll.commands.fakeop")) {
				if(args.length == 1) {
					try {
						Player vic = Bukkit.getPlayer(args[0]);
						vic.sendMessage("�7�o[Console] Opped Player " + vic.getName());
						p.sendMessage(Start.instance.prefix + "�aDer Spieler denkt jetzt dass er OP Rechte hat.");
					} catch (Exception e) {
						p.sendMessage(Start.instance.prefix + "�cDer Spieler ist leider nicht Online!");
					}
				}
			} else {
				p.sendMessage(Start.instance.prefix + "�cDu hast leider nicht die Rechte um diesen Befehl auszuf�hren!");
			}
		}
	}
	
	

}
